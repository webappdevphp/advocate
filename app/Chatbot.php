<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chatbot extends Model
{
    //
    protected $fillable = ['topic','name','email','response','user_id'];
}
