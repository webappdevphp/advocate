<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestAttorney extends Model
{
    //
    protected $fillable = ['user_id','response','terms','allowtosharedetail','category_id','attorney'];
}
