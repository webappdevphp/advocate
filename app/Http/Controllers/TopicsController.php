<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Chatbot;
use Mail;
use App\User;
use Auth;
use Hash;
use Session;

class TopicsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = '';
        if($request->isMethod('post')){
            $search = $request->get('search');
        }
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_PORT => "5002",
            CURLOPT_URL => "http://www.zestyguys.com:5002/search_topic",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\"term\":\"$search\"}",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "cache-control: no-cache",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        $message  = '';
        if ($err) {
            echo "cURL Error #:" . $err;
            $message = 'Error, Try again after some time.';
        } else {
            $message = '';
        }


        if($request->isMethod('post')){
            $status = true;
            $search = $request->get('search');
            if($message != ''){
                $status = false;

            }
            return response()->json(array('search' => $search,'result' => $response, 'status' => $status,'message' => $message ));

        }
        $topics  = (array)json_decode($response);
        $topics = (array)$topics['Topics'];
        return view('topic.index',compact('topics','message'));
    }


    public function chatbot($id){
        // $topic = 'Changing my current workers’ compensation doctor';//array($id =>  $topics[$id]);
        $topic = $this->getTopicDetail($id);
        Session::put('topic', $id);
        $user_id = (Auth::user())? Auth::user()->id : '';
        //echo $user_id;
        if($topic){
			$issue_id = time().'-'.$id;
            $topic = json_decode($topic);
            $detail =$topic->Detail;
            $name = $detail->name;
        	$url  = "http://www.zestyguys.com:5002/chat_app";

			if(str_replace(url('/'), '', url()->previous()) == '/topic'){
				$sessionId = time() . '-' . $id;
				$url  = "http://www.zestyguys.com:5002/begin_chat";
				Session::put('chatBotId', $sessionId);

			}
			if(Session::get('chatBotId') == ''){
				$sessionId  = time().'-'.$id;
				// $_SESSION['chatBotId'] = $sessionId;
				Session::put('chatBotId',$sessionId);

			}else{
				$sessionId = Session::get('chatBotId');

			}
			$chatbotArray  = array(
                CURLOPT_PORT => "5002",
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS =>  "{\"session_id\":\"$sessionId\",\"issue_id\":\"$id\",\"user_id\":\"$user_id\",\"data\":{\"message\":\"$name\"}}",
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json",
                    "cache-control: no-cache",
                ))
				;
            $curl = curl_init();
            //$sessionId  = time().'-'.$id;
            curl_setopt_array($curl, $chatbotArray );
            // print_r($curl);
            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
			$message = '';
            $result  = json_decode($response);
			
            if ($err ) {
                $message = "Something went wrong try again";
            } 
			if (empty($result)){
                $message = "Something went wrong try again";
            }
			/*
			 echo $sessionId;echo '<br />';
			echo $url .'<br />';
			echo $id.'<br/>';
			echo $name;
            print_r($result);exit;  */
            return view('topic.chatbot', compact('topic', 'id', 'result', 'message', 'sessionId', 'detail','user_id'));

        }else{
            $message = "Something went wrong try again";
            $result = array();
            return view('topic.chatbot',compact('topic','id','result','message','sessionId'));
        }

    }


    public function chatbotanswer(Request $request){
        //$topic = 'Changing my current workers’ compensation doctor';//array($id =>  $topics[$id]);
        $sessionId  = $request->sessionId;
        $answer  = $request->answer;
        $user_id  = $request->user_id;

        $curl = curl_init();
        //$sessionId  = time().'-'.$id;
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "5002",
            CURLOPT_URL => "http://www.zestyguys.com:5002/chat_app",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\"session_id\":\"$sessionId\",\"user_id\":\"$user_id\",\"data\":{\"message\":\"$answer\"}}",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "cache-control: no-cache",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        $message = '';
        $result;
        if ($err) {
            $message = "Something went wrong try again";
			exit;
        } else {
            $result  = $response;
        }
        return response()->json(array('sessionId' => $sessionId, 'result' => $response, 'message' => $message));

        exit;

        //return view('topic.chatbot',compact('topic','id','result','message','sessionId'));
    }

    public function chatstore(Request $request,$id){

        $data  = $request->except('_token');
        $sessionId = Session::get('chatBotId');
        $user = User::find(Auth::id());
        $data['name'] = $user->name;
        $data['user_id'] = $user->id;
        $topic = $this->getTopicDetail($id);
        $data['email']  = $user->email;
        $data['topic'] = $id;
        $data['response'] = json_encode($data);
        $chatboat  = Chatbot::create($data);
        $message = 'Successfull';

        /* Mail::send('mail.formfill', $data, function ($message) {
            $message->to('webappdevs.php@gmail.com', 'Admin')->subject
            ('Request For attorny');
            //$message->from('xyz@gmail.com', 'Virat Gandhi');
        }); */
		Session::put('chatBotId', '');
        return response()->json(array('sessionId' => $sessionId, 'result' => $chatboat, 'message' => $message));
        exit;
        // return redirect('attorny/'.$id.'/'. $chatboat->id);
    }

    public function attorny(Request $request,$topic,$id){
        echo '<h2>'.$topic.'</h2>';
        echo '<h2>'.$id.'</h2>';

    }


    public function getTopicDetail($id){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_PORT => "5002",
            CURLOPT_URL => "http://www.zestyguys.com:5002/topic_detail",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\"id\":\"$id\"}",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "cache-control: no-cache",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        if ($err) {
            return false;
        } else {
            return $response;
        }

    }

    public function makeLogin(Request $request){
        $credentials = array(
            'email' => $request->get('email'),
            'password' => $request->get('password'),
        );
        if($request->get('mode') == 'login'){
            $user = User::where("email", $request->email)->first();
            if ($user) {
                if (Hash::check($request->password, $user->password)) {
                    if (Auth::attempt($credentials)) {
                        return response()->json(array('result' => 'login', 'message' => 'login', 'status' => true));
                        exit;
                    } else {
                        return response()->json(array('result' => 'We can not found your account', 'meesage' => $request->get('name'), 'status' => false));
                        exit;
                    }
                } else {
                    return response()->json(array('result' => 'We can not found your account', 'meesage' => $request->get('name'), 'status' => false));
                    exit;

                }
            } else {
                return response()->json(array('result' => 'We can not found your account', 'meesage' => $request->get('name'), 'status' => false));
                exit;

            }

        }else{
            $data = $request->except('mode');
            $rules = array(
                'name' => 'required|max:255',
                'email' => 'required|email|max:255|unique:users',
                'password' => 'required|min:6',
            );
            $validator = \Validator::make($data, $rules, []);
            if ($validator->fails()) {
                $validation = $validator;
                $status = false;
                $code = 400;
                $msgArr = $validator->messages()->toArray();
                $messages = reset($msgArr)[0];
                return response()->json(array('result' => $messages, 'meesage' => $request->get('name'), 'status' => false));
                exit;

            }else{

                $user = User::create([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),

                ]);
                $user->assignRole('US');
                return response()->json(array('result' => 'register', 'message' => 'login', 'status' => true));
                exit;
            }



        }

    }
}
