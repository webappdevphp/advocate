<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Session;
use Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }




    public function authenticate(Request $request)
    {
        $credentials = array(
            'email' => $request->get('email'),
            'password' => $request->get('password'),

        );

        $user = User::where("email", $request->email)->first();

        if ($user) {
            if (Hash::check($request->password, $user->password)) {

                if (Auth::attempt($credentials)) {

                    Session::flash('flash_success', "Login Success !!");
                    if (Auth::user()->hasRole('SU')) {
                        return redirect('/admin');
                    }

                    if (Session::get("chatBotId")) {
                        return redirect('/topic/'.Session::get("topic"));
                    }
                    if(Session::get("previousUrl")){
                        return redirect()->route(Session::get("previousUrl"));
                    }
                } else {
                    Session::flash('flash_error', "Invalid Username or Password");
                }

            } else {
                Session::flash('flash_error', "Invalid Username or Password");
            }
        } else {
            Session::flash('flash_error', "Invalid Username or Password");
        }


        return redirect()->back()->withInput();

    }


}
    // public function makeLogin(Request $request){
    //     $credentials = array(
    //         'email' => $request->get('email'),
    //         'password' => $request->get('password'),
    //     );
    //     $user = User::where("email", $request->email)->first();
    //     if ($user) {
    //         if (Hash::check($request->password, $user->password)) {
    //             if(Auth::attempt($credentials) ){
    //                 return response()->json(array('result' => 'login'));
    //                 exit;
    //             }else{
    //                 return response()->json(array('result' => 'We can not found your account'));
    //                 exit;
    //             }
    //         }else{
    //             return response()->json(array('result' => 'We can not found your account'));
    //             exit;

    //         }
    //     }else{
    //         return response()->json(array('result' => 'We can not found your account'));
    //         exit;

    //     }
    // }
