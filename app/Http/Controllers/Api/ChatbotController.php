<?php

namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;
class ChatbotController extends Controller
{
    public function index(Request $request){
        //print_r($_FILES);
        //dd($request->all());
		$emails = explode(',',$request->get('to'));
		$cc =  explode(',',$request->get('cc'));
		$bcc =  explode(',',$request->get('bcc'));
        //$emails = array('webappdevs.php@gmail.com', 'prganvani@gmail.com');
		$data = array('body' => $request->get('body'));
		$files = $request->file();
        Mail::send('mail.sendPdf', $data, function ($message) use ($emails,$files,$cc,$bcc) {
            $message->to($emails)->subject('This is test e-mail')
			->cc($cc)
			->bcc($bcc);
			foreach ($files as $file) {
				$message->attach($file->getRealPath(),[
                    'as' => $file->getClientOriginalName(),
                    'mime' => $file->getClientMimeType(),
                ]); // attach each file
			}
        });
        if(!empty(Mail::failures()))
		{
			return response()->json(array('status'=>false,'code'=>200,'message'=> 'Try again'));
		}else{
			return response()->json(array('status'=>true,'code'=>200,'message'=> 'Mail send'));
		}
        exit;

    }
}
?>
