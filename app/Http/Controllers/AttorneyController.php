<?php

namespace App\Http\Controllers;

use App\Attorney;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use App\RequestAttorney;

class AttorneyController extends Controller
{
    public function __construct()
    {
        Session::put('previousUrl', 'attorny-request');
        $this->middleware('auth');
        $this->user =  \Auth::user();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = array(
            'question1' => array(
                'question' => 'What is your name?',
                'type' =>  'text',
            ),
			'question2' => array(
                'question' => 'Please Select Category',
                'type' =>  'select',
                'options' => array(
                    'Case-Category-1' => 'Case-Category-1',
                    'Case-Category-2' => 'Case-Category-2',
                    'Case-Category-3' => 'Case-Category-3',
                    'Case-Category-4' => 'Case-Category-4',
                    'Case-Category-5' => 'Case-Category-5'
                )
            ),
            'question3' => array(
                'question' => 'Please provide your phone number',
                'type' =>  'text',
            ),
            'question4' => array(
                'question' => 'What is your email address?',
                'type' =>  'email',
            ),
            'question5' => array(
                'question' => 'Please provide the city name where you live.',
                'type' =>  'text',
            ),'question6' => array(
                'question' => 'Thank You',
                'type' =>  'noresponse',
            )


        );
        return view('attorney.index',compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $data = array();
        $user_id = Auth::id();
        $dataTostore  = $request->except('_token');
        $data['response'] = json_encode($dataTostore);
        $data['category_id'] = $request->get('Please_Select_Category');
        $data['user_id'] = $user_id;
        $data['terms'] = 0;
        $data['allowtosharedetail'] = 0;
        $data['attorney'] = '';

        $requestAttorny  = RequestAttorney::create($data);
        // return \Redirect::route('accept-term', $requestAttorny->id);
        //return redirect()->route('accept-term', ['id' => $requestAttorny->id]);
        // return redirect('accept-term/'.$requestAttorny->id);
        return response()->json(array('requestAttorneyId' => $requestAttorny->id, 'status'=>true));
        exit;
    }

    public function acceptTerm($id){
        $requestAttorney = RequestAttorney::find($id);
        return view('attorney.terms',compact('id','requestAttorney'));
    }

    public function attorneyList($id){
        $requestAttorney = RequestAttorney::find($id);
        $attorneyList = Attorney::whereIN('id',explode(',',$requestAttorney->attorney))->get();
        return view('attorney.list',compact('id','requestAttorney','attorneyList'));
    }
    public function acceptTermStore(Request $request){
        $requestAttorney = RequestAttorney::find($request->get('attorneyRequestId'));
        $requestAttorney->terms = ($request->get('terms') == 'on')? 1 : 0 ;
        $requestAttorney->allowtosharedetail = ($request->get('allowtosharedetail') == 'on') ? 1 : 0;
        $attorney  = Attorney::where('category',$requestAttorney->category_id)->inRandomOrder()->limit(3)->get()->pluck('id','id');
        $attorneyList = '';
        foreach($attorney as $value)
        {
            $attorneyList .= $value.',';
        }
        $requestAttorney->attorney = $attorneyList;//implode(',',(array) $attorney);
        $requestAttorney->update();
        $attorneyList = Attorney::whereIN('id', explode(',', $attorneyList))->get();
        return response()->json(array('requestAttorneyId' => $requestAttorney->id,'attorneyList' => $attorneyList, 'status'=>true));
        exit;
        //return \Redirect::route('attorneyList',$request->get('attorneyRequestId'));
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Attorney  $attorney
     * @return \Illuminate\Http\Response
     */
    public function show(Attorney $attorney)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Attorney  $attorney
     * @return \Illuminate\Http\Response
     */
    public function edit(Attorney $attorney)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Attorney  $attorney
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Attorney $attorney)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Attorney  $attorney
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attorney $attorney)
    {
        //
    }
}
