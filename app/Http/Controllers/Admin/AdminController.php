<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\RequestAttorney;


class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        return view('admin.dashboard');
    }

    public function listRequestAttorney(Request $request){
        $keyword = $request->get('search');
        $perPage = 15;

        // if (!empty($keyword)) {
        //     $chatbots = RequesAttorney::where('name', 'LIKE', "%$keyword%")->orWhere('topic', 'LIKE', "%$keyword%")->orWhere('email', 'LIKE', "%$keyword%")->paginate($perPage);
        // } else {
        //     $chatbots = Chatbot::orderBy('id', 'desc')->paginate($perPage);
        // }

        $requesAttorney = RequestAttorney::join('users','users.id','request_attorneys.user_id')->select('request_attorneys.*', 'users.id as user_id','users.name as name')->orderBy('request_attorneys.id','desc')->paginate($perPage);

        //$chatbots = Chatbot::->get();
        return view('admin.attorney.index', compact('requesAttorney'));

    }

    public function listRequestAttorneyDetail ($id){
        $requesAttorney = RequestAttorney::join('users','users.id','request_attorneys.user_id')->find($id);

        return view('admin.attorney.show', compact('requesAttorney'));

    }
}
