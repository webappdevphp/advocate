<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Advertise;
use Session;

class AdvertisementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 15;

        if (!empty($keyword)) {
            $advertise = Advertise::where('name', 'LIKE', "%$keyword%")->orWhere('jobtitle', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $advertise = Advertise::paginate($perPage);
        }

        return view('admin.advertisement.index', compact('advertise'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.advertisement.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'jobtitle' => 'required',
            'image' => 'mimes:jpeg,jpg,png,gif|required|max:10000',
            'description' => 'required'
        ]);

        $data = $request->except('image');
        $file = $request->file('image');

        $timestamp = str_replace([' ', ':'], '-', \Carbon\Carbon::now()->toDateTimeString() . uniqid());

        $name = $timestamp . '-' . $file->getClientOriginalName();

        $file->move(public_path() . '/uploads/', $name);

        $data['image']  = $name;
        $data['status']  = 1;

        $advertise = Advertise::create($data);

        Session::flash('flash_success', 'Advertise added!');

        return redirect('admin/advertisement');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $advertise = Advertise::findOrFail($id);

        return view('admin.advertisement.show', compact('advertise'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $advertise = Advertise::findOrFail($id);

        return view('admin.advertisement.edit', compact('advertise'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'jobtitle' => 'required',
            'description' => 'required'
        ]);

        $data = $request->except('image');

        if($request->file('image')){
            if(file_exists('uploads/'.$request->old_image)){
                unlink('uploads/'.$request->old_image);
            }
            
            $timestamp = str_replace([' ', ':'], '-', \Carbon\Carbon::now()->toDateTimeString() . uniqid());

            $name = $timestamp . '-' . $request->file('image')->getClientOriginalName();

            $request->file('image')->move(public_path() . '/uploads/', $name);

            $data['image']  = $name;
        }

        Advertise::find($id)->update($data);

        return redirect('admin/advertisement')->with('flash_success','Advertise updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $advertise = Advertise::find($id);

        if(file_exists('uploads/'.$advertise->image)){
            unlink('uploads/'.$advertise->image);
        }

        $advertise->delete();

        return redirect('admin/advertisement')->with('flash_success','Advertise deleted');
    }
}
