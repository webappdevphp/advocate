@extends('layouts.front')

@section('title','Home')

@section('content')
    <div class="fw-home-first-row fw-home-row">
	  <div class="fw-home-first-row-cover">
	    <div class="fw-home-first-row-banner">
	      <div class="container">
	        <h1 class="fw-banner-header">A gift for you and your future:</h1>
	        <h1 class="fw-banner-header">Create or update your will today</h1>
	        <h1 class="fw-banner-header">- fast and 100% free.</h1>
	        <a class="btn btn-primary fw-btn-home" rel="nofollow" data-method="post" href="https://www.freewill.com/wills">Get Started</a>
	      </div>
	    </div>
	  </div>
	</div>

	<div class="fw-home-row fw-home-reasons-row">
	  <div class="container">
	    <h1 class="fw-home-reasons-title">Reasons to Use FreeWill</h1>
	    <div class="row">
	      <div class="col-md-1"></div>
	      <div class="col-md">
	        <img class="fw-img-height-40" src="{{ asset('front/images/img1.png')}}" alt="Home justice logo">
	        <h3 class="fw-home-reasons-subtitle">To Be Thoughtful</h3>
	        <p class="fw-home-reasons-text">Make important decisions where they belong - at home.</p>
	      </div>
	      <div class="col-md">
	        <img class="fw-img-height-40" src="{{ asset('front/images/img2.png')}}" alt="Home group logo">
	        <h3 class="fw-home-reasons-subtitle">To Be Kind</h3>
	        <p class="fw-home-reasons-text">Support the people and causes that mean the most to you.</p>
	      </div>
	      <div class="col-md">
	        <img class="fw-hourglass-log fw-img-height-40" src="{{ asset('front/images/img3.png')}}" alt="Home hourglass logo">
	        <h3 class="fw-home-reasons-subtitle">To Be Savvy</h3>
	        <p class="fw-home-reasons-text">Save time, money, and stress for you and your loved ones.</p>
	      </div>
	      <div class="col-md-1"></div>
	    </div>
	  </div>
	</div>

	<div class="fw-home-third-row">
	  <div class="container">
	    <div class="row fw-home-numbers-container">
	      <div class="col-md-4 fw-home-numbers-column">
	        <h2 class="fw-home-numbers-title">FreeWill by the Numbers</h2>
	        <h3 class="fw-home-number" data-count="20102" data-type="number">20,102</h3>
	        <p>wills created</p>
	        <h3 class="fw-home-number" data-type="currency" data-count="226844000">$226,844,000</h3>
	        <p>committed to charity</p>
	        <h3 class="fw-home-number" data-count="98" data-type="percent">98%</h3>
	        <p>positive feedback</p>
	      </div>
	    </div>
	  </div>
	</div>

	<div class="fw-home-row fw-home-questions-row">
	  <div class="container">
	    <h1 class="fw-home-questions-title">Got questions? We've got the answers</h1>
	    <div class="row">
	      <div class="col-md-4">
	        <img class="fw-img-height-40" src="{{ asset('front/images/img8.png')}}" alt="Home gavel logo">
	        <h3 class="fw-home-question-main">Is it legally effective?</h3>
	        <p class="fw-home-question-text">Yes! FreeWill is 100% legal in all 50 states. FreeWill was built with extensive input from top trusts &amp; estates experts.</p>
	      </div>
	      <div class="col-md-4">
	        <img class="fw-img-height-40" src="{{ asset('front/images/img9.png')}}" alt="Home edit logo">
	        <h3 class="fw-home-question-main">Can I change my will?</h3>
	        <p class="fw-home-question-text">Yes! You can create a new will to replace your old one. Just make sure to destroy the old will, and sign the new one with witnesses.</p>
	      </div>
	      <div class="col-md-4">
	        <img class="fw-img-height-40" src="{{ asset('front/images/img10.png')}}" alt="Home gift logo">
	        <h3 class="fw-home-question-main">Why is FreeWill free?</h3>
	        <p class="fw-home-question-text">FreeWill is made possible by the support of many nonprofit organizations. Many of our users choose to leave a portion of their estate to charity, and we hope you’ll consider doing the same.</p>
	      </div>
	    </div>
	    <div class="fw-home-questions-row-bottom">Still have questions? Explore our <a href="https://www.freewill.com/learn">Learn</a> section, <a href="https://www.freewill.com/faq">FAQs</a> or <a href="mailto:help@freewill.com">contact us</a>.</div>
	  </div>
	</div>

    <div class="fw-home-row fw-home-bottom-row">
        <div class="container text-center">
            <h2>Make your will with <b>FreeWill</b> today.</h2>
            <a class="btn btn-primary fw-btn-home" rel="nofollow" data-method="post" href="https://www.freewill.com/wills">Get Started</a>
        </div>
    </div>


</div>

@endsection
@push('script')
<script>
    $(document).ready(function(){
        function formatAsCurrency(num) {
        return "$" + (Math.round(num / 1000) * 1000).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        function formatAsPercent(num) {
        return Math.round(num).toString() + "%";
        }

        function formatAsNumber(num) {
        return Math.round(num).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        $(window).scroll(function(){
        var offsetTop = $('.fw-home-numbers-title').offset().top - window.innerHeight;
        if ($(window).scrollTop() > offsetTop) { // when the number has come into view
            $(window).unbind('scroll');
            $('.fw-home-number').each(function(){
            var numberElement = $(this);
            $({count: 0}).animate({count: $(this).attr("data-count")}, {
                duration: 2000,
                easing: 'swing',
                step: function(now) {
                if( numberElement.attr("data-type") === "currency" ) {
                    numberElement.text(formatAsCurrency(now));
                } else if ( numberElement.attr("data-type") === "percent" ) {
                    numberElement.text(formatAsPercent(now));
                } else if ( numberElement.attr("data-type") === "number" ){
                    numberElement.text(formatAsNumber(now));
                }
                },
            });
            });
        }
        });
    });
</script>
@endpush
{{-- <!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Laravel
                </div>

                <div class="links">
                    <a href="https://laravel.com/docs">Documentation</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://nova.laravel.com">Nova</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
            </div>
        </div>
    </body>
</html>
 --}}
