<div class="fw-navbar-container">
  		<div class="container">
    		<nav class="navbar navbar-toggleable-md navbar-inverse fw-navbar-inverse">
        		<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          			<span class="navbar-toggler-icon"></span>
        		</button>

	      		<a class="navbar-brand" href="{{ url('/')}}">
	          		<img class="fw-home-brand-logo" src="{{asset('front/images/Logo.png')}}" alt="Keeleg">
	  			</a>

		      	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		        	<ul class="navbar-nav ml-auto d-flex align-items-center">
		          		<!--<li class="nav-item fw-test-log-in">
							<a class="nav-link" href="#">For nonprofits</a>
                        </li> -->
						<li class="nav-item fw-test-log-in">
							<a class="nav-link" href="{{url('/topic')}}">Topic</a>
                         </li>
                         <li class="nav-item fw-test-log-in">
							<a class="nav-link" href="{{route('attorny-request')}}">Request Attorney Assistance</a>
                         </li>
							@if(!Auth::check())
							  <p></p>
							@else

                            <li class="nav-item dropdown">

                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ url('/logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endif
		        	</ul>
		      	</div>
    		</nav>
  		</div>
	</div>
