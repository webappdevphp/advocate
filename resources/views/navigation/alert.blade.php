@if(session()->has('flash_error'))
    <div class="alert alert-error">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {!! session()->get('flash_error') !!}
    </div>
@endif

@if(session()->has('flash_success'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {!! session()->get('flash_success') !!}
    </div>
@endif