@extends('layouts.front')

@section('title','Home')
@push('css')
<link type="text/css" href="{{ asset('dist/jquery.convform.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('dist/chatbot.custom.css')}}">
@endpush
@section('content')
<div class="fw-auth">
    <div class="container">
        <div class="row">
        @if($message == '')

			<div class="module col-md-12" style="min-height: 500px">
				<div class="subtitle">
					<h3>Topic: {{$detail->name}}</h3>
				</div>
				<div>
					<div class="card no-border chatapp">
						<div id="chat" class="conv-form-wrapper">
                            <form action="{{url('chatstore/'.$id)}}" method="POST" class="hidden">
                                <?php
								
                                    if($result[0]->type == 'button'){
                                        $optionHtml = '';
                                        foreach($result[0]->options as $key => $value){
                                            $optionHtml .= '<option value="'.$value.'">'.$value.'</option>';
                                        }

                                        ?>
                                        <select data-conv-question="{{$result[0]->message}}" name="{{$result[0]->message}}">
                                            <?php echo $optionHtml;?>
	                                    </select>
                                    <?php }else {
										
                                 ?>
								<input type="text" data-conv-question="{{$result[0]->message}}" data-no-answer="<?php echo (count($result) > 1) ? true : false ?>">
								<?php if(count($result) > 1) { ?>
                                <input type="text" name="name" data-conv-question="Enter your name">
									<?php }
									}

                                ?>
                                {{-- <input id="test" type="text" data-conv-question="Message"  data-no-answer="true"> --}}
							</form>
						</div>
					</div>
				</div>
            </div>
            {{-- <h2 id="test" style="display:none;"> Test</h2> --}}
		@else
			<h2 style="color:red">{{$message}}</h2>
		@endif
	    </div>
	</div>
	</div>

@endsection
@push('script')
{{-- <script type="text/javascript" src="{{ asset('jquery-1.12.3.min.js')}}"></script> --}}
<script type="text/javascript" src="{{ asset('dist/autosize.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('dist/jquery.convform.js')}}"></script>
@endpush


@push('script')
<script>
    var count = 0;
    var status = 'in_progress';
    var url = "{{route('chatbotanswer')}}";
    var checkauth = '{{Auth::check()}}'

    $(function(){
        function SubmitForm(){
//            alert('dfsd');
            // $.ajaxSetup({
            //     header:$('meta[name="_token"]').attr('content')
            // })
            var inputElem = document.createElement('input');
            inputElem.type = 'hidden';
            inputElem.name = '_token';
            inputElem.value = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
            $('.hidden').append(inputElem);
            var formData = $('.hidden').serialize();
            var url = $('.hidden').attr("action")
            
            $.ajax({
                type:"POST",
                url:url,
                data:formData,
                dataType: 'json',
                success: function(data){
                    
                },
                error: function(data){

                }
            })
        }
        // $("#login").click(function(){
        //     $.ajax({
        //             type: 'POST',
        //             url: ,
        //             data: data,
        //             success: function(response){

        //             }
        // });
        var loginStatus = false;
        var registrationStatus = false;
        var loginCount = 0;
        var registrationCount = 0;
        var formdata = {};
        var convForm = $('#chat').convform(
            {eventList:{onInputSubmit: function(convState, ready) {

            console.log('input is being submitted...');
            var name = convState.current.input.name;
			//console.log(convState.answers)
//			console.log("{{route('register')}}"+"?name="+convState.answers.name.value)
            if(name === 'name')
                formdata.name = convState.current.answer.value;

            if(loginStatus || registrationStatus){
                if(name == 'email')
                    formdata.email = convState.current.answer.value;
                if(name == 'password')
                    formdata.password = convState.current.answer.value;
            }
                if(status == 'in_progress'){


                    var data = {
                        sessionId : '{{$sessionId}}',
                        _token : '<?php echo csrf_token() ?>',
                        answer : convState.current.answer.text,
                        user_id : '{{$user_id}}'
                    }

                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: data,
                        success: function(response){
                            if(response.message == ''){
                                var result = JSON.parse(response.result)
                                if(result.length == 1){
									convState.current.next  = true;
									setTimeout(ready, Math.random()*500+100);
                                    result = result[0];
                                    //console.log(result.type,result.message,result.options);
                                    if(result.message != 'login' && result.message != 'register' ){
                                        if(result.type == 'button')
                                        {
                                            var message = result.message
                                            let newArr = result.options.map((val, index, arr) => {
                                                return {value:val,text:val}
                                            });
                                            var jsonFornewState = {
                                                type: 'select',
                                                name: message,
                                                questions: [message],
                                                answers:newArr,

                                            }
                                        }
                                        if(result.type == 'text'){

                                            var message = result.message
                                            var jsonFornewState = {
                                                type: 'text',
                                                questions: [message],
                                            }
                                        }
                                        convState.current.next = convState.newState(jsonFornewState);
                                    }else if(checkauth){
                                        convState.current.next  = true;
                                        setTimeout(ready, Math.random()*500+100);
                                        var data = {
                                            sessionId : '{{$sessionId}}',
                                            _token : '<?php echo csrf_token() ?>',
                                            answer : 'OnSuccess',user_id : '{{$user_id}}',
                                        }
                                        setTimeout(ready, Math.random()*500+100);
                                        $.ajax({
                                            type: 'POST',
                                            url: url,
                                            data: data,
                                            success: function(response){
                                                if(response.message == ''){
                                                    var result = JSON.parse(response.result)
                                                    if(result.length == 1){
                                                        result = result[0];
                                                        //console.log(result.type,result.message,result.options);
                                                        if(result.message != 'login' && result.message != 'register' ){
                                                            if(result.type == 'button')
                                                            {
                                                                var message = result.message
                                                                let newArr = result.options.map((val, index, arr) => {
                                                                    return {value:val,text:val}
                                                                });
                                                                var jsonFornewState = {
                                                                    type: 'select',
                                                                    name: message,
                                                                    questions: [message],
                                                                    answers:newArr,

                                                                }
                                                            }
                                                            if(result.type == 'text'){

                                                                var message = result.message
                                                                var jsonFornewState = {
                                                                    type: 'text',
                                                                    questions: [message],
                                                                }
                                                            }
                                                            setTimeout(ready, Math.random()*500+100);
                                                            convState.current.next = convState.newState(jsonFornewState);
                                                        }
                                                    }
                                                }
                                            },
                                            async:false
                                        });
                                        setTimeout(ready, Math.random()*500+100);
                                    } else{
                                        if(result.message == 'login'){
                                            if(!checkauth)
                                                window.location = "{{route('login')}}";
                                        }
                                        if(result.message == 'register'){
                                            if(!checkauth)
                                                window.location = "{{route('register')}}"+"?name="+convState.answers.name.value;
                                        }
                                    }
                                }else{

									var tick = function(i) {
										return function() {
										//console.log(i);
										}
									};
									setTimeout(tick(0), 2000);

                                    for(i=0;i < result.length;i++){
                                        if(result[i].status == 'completed'){
                                            //convState.current.next = false;

                                            // console.log('dfd');
                                            // $('#test').show();
                                            // $("#chat").find('form.hidden').append('<input id="test" type="text" data-conv-question="Message"  data-no-answer="true">');
                                        	//emulating random response time (100-600ms)
                                            //$("#messages").append('<div class="message to ready">'+result[i].message+'</div>')
                                            var message = result[i].message;
											console.log('if')
											//console.log('i',i)
											//console.log('status',result[i].status);
											//console.log('message',message);
                                            var count = 0;
											var tickTok = function(message) {
												return function() {
                                                    $( ".typing" ).hide();
                                                    //console.log('placeholder',message);
													$('#userInput').prop("disabled", true);
													$("#messages").append('<div class="message to ready" id="msg">'+message+'</div>')
													/* var mydiv = document.getElementById("messages");
													var aTag = document.createElement('a');
													aTag.setAttribute('href',"#msg");
													aTag.setAttribute('id',"msgA");
													aTag.innerHTML = "";
													mydiv.appendChild(aTag);
													$("#messages a").trigger('click'); 
													$('html, body').animate({
														'scrollTop' : $("#msg").position().top
													});*/
													$("#chat").find('#messages').stop().animate({scrollTop: $("#chat").find('#messages')[0].scrollHeight}, 600);
												}
											};
											setTimeout(tickTok(message), 1000);
                                            /* var placeHolder = setInterval(function() {
                                                $("#messages").append('<div class="message to ready">'+message+'</div>')
												console.log('placeholder',message);
                                                //SubmitForm();

                                                count++;
                                                if(count > 3)
                                                    clearInterval(placeHolder);
                                            }, 1000); */
// {{--
//                                              var jsonFornewState = {
//                                                 type: 'text',
//                                                 questions: [result[i].message],
//                                                 noAnswer:true,
//                                             }
//                                             // convState.current.next  = true;
//                                             convState.current.next = convState.newState(jsonFornewState); --}}
											
                                        }else{


                                            var count = 0;
											//console.log(result[i]);
                                            var message = result[i].message;
											console.log('else')
											//console.log('i',i)
											//console.log('status',result[i].status);
											//console.log('message',message);
                                            /* var placeHolderName = setInterval(function() {
												console.log('placeholder',message);
                                                if(count == 0){
													$("#messages").append('<div class="message to ready">'+message+'</div>')

                                                }
                                                count = count + 1; //i forget if ++ works...
                                                if(count > 3)
                                                    clearInterval(placeHolderName);

                                            }, 1000); */

											//$('#messages div:last').remove();
											var tick = function(data,i) {
												return function() {

													if(data.type == 'button')
													{	console.log('else if in');
														$( ".typing" ).hide();
														console.log(data.message);
														console.log(data.options);
														setTimeout(ready, Math.random()*500+100);
														convState.current.next  = true;
														var message = data.message
														console.log(message);
														$("#messages").append('<div class="message to ready">'+message+'</div>')
														let newArr = data.options.map((val, index, arr) => {
															return {value:val,text:val}
														});
														$(".options").show();
														var jsonFornewState = {
															type: 'select',
															name: message,
															questions: [message],
															answers:newArr,

														}
														convState.current.next = convState.newState(jsonFornewState);
														//setTimeout(ready, Math.random()*500+100);
													}else{
														/*$(".options").hide();
														var message = result.message
														
														
														$( ".typing" ).hide();
														
														$("#chat").find('#messages').stop().animate({scrollTop:
														$("#chat").find('#messages')[0].scrollHeight}, 600);*/
														console.log(data)
														var message = data.message
														var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
														if(regexp.test(message)){
															var url  = message;
															message  = '<a href="'+url+'" target="_blank">Download Pdf</a>';
															$("#messages").append('<div class="message to ready">'+message+'</div>')
														}else if(data.type == 'text' && result.length -1 == i){
															$( ".typing" ).hide();
															console.log('dynamix',i,message)
															setTimeout(ready, Math.random()*500+100);
															convState.current.next  = true;
															$("#messages").append('<div class="message to ready">'+message+'</div>')
															var message = data.message
															var jsonFornewState = {
																type: 'text',
																questions: [message],
															}
															convState.current.next = convState.newState(jsonFornewState);
															$("#chat").find('#messages').stop().animate({scrollTop: $("#chat").find('#messages')[0].scrollHeight}, 600);
														}else {
															console.log('static',i,message)
															$("#messages").append('<div class="message to ready">'+message+'</div>')
														} 
														
													}
												}
											};
											setTimeout(tick(result[i],i), 1000);
                                            // result[i].message]


                                        }
										
										$("#chat").find('#messages').stop().animate({scrollTop: $("#chat").find('#messages')[0].scrollHeight}, 600);

                                    }
									$( ".typing" ).remove()
									
									var elem = document.getElementById('messages');
									elem.scrollTop = elem.scrollHeight
									
									
									
                                }

                            }else{
                                alert(response.message)
                            }
                        },

                        async:false
                    });

                    }else{

                        /*convState.current.next = false;
                        console.log('dfd');
        // 	//emulating random response time (100-600ms)
                        setTimeout(ready, Math.random()*500+100); */
                    }

        },
         onSubmitForm: function(convState) {
            console.log('The form is being submitted!');
            // var inputElem = document.createElement('input');
            // inputElem.type = 'hidden';
            // inputElem.name = '_token';
            // inputElem.value = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
            // convState.form.append(inputElem)
            // convState.form.submit();
        }
        }});
    });
</script>
@endpush
