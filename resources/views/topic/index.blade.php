@extends('layouts.front')

@section('title','Home')

@section('content')
{{-- <div class="module " style="min-height: 500px">
    <h1 style="text-align: center;line-height: 100px">Welcome to {{ config('app.name') }}  </h1>
    <div>
        <input type="text" id="searchTopic" />
    </div>
    <div >
        <ul id="topicList">
            @foreach($topics as $key => $topic)
             <li id="{{$topic->id}}"><a href="{{url('topic/'.$topic->id)}}">{{$topic->name}}</a></li>
            @endforeach
        </ul>
    </div> --}}
<div class="fw-auth">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
                <a href="{{url('/')}}">
					<i class="fa fa-angle-left"></i> Back to Home
				</a>
				<div class="fw-auth-section whiteback" style="min-height: 500px;">
					<h1>Search Topic</h1>
					<input type="text" title="Search Topic" class="form-control" id="searchTopic">
					<ul id="topicList">
						@foreach($topics as $key => $topic)
							<li id="{{$topic->id}}">
								<a href="{{url('topic/'.$topic->id)}}" tittle={{$topic->name}}>{{$topic->name}}</a>
							</li>
						@endforeach
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
@push('script')
<script>
    var topicUrl = "{{url('topic/')}}"
    $(document).ready(function () {
       $('#searchTopic').on('input',function(e){
            if($(this).val() != ''){
                var url = "{{ url('gettopic/')}}"
                $.ajax({
                    type:'POST',
                    url:url,
                    data:{_token : '<?php echo csrf_token() ?>', search : $(this).val()},
                    success:function(data){
                        $('#topicList').html('');
                        if(data.status){
                            var response  = JSON.parse(data.result);
                            console.log(response.Topics);
                            // data.result.each(function(item) {
                            // $("#topicList").append("<li>" + item + "</li>");
                            // });
                             $.each(response.Topics, function(index, value) {
                                 $("#topicList").append("<li> <a href='"+topicUrl+'/'+value.id+"'>" + value.name + "</a></li>");
                            });
                        }
                    }
                });
            }
        });
   });
</script>
@endpush
