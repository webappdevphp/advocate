@extends('layouts.front')

@section('title','Home')
@push('css')
<link type="text/css" href="{{ asset('dist/jquery.convform.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('dist/chatbot.custom.css')}}">
@endpush
@section('content')
<div class="col-md-12" style="min-height: 500px">
    <h1 style="text-align: center;line-height: 100px">Welcome to {{ config('app.name') }}  </h1>

    @if($message == '')
    <div>
        <h3>{{$detail->name}}</h3>

    </div>
    <div >
        <?php //print_r($result); ?>
        <div id="chat">
            <form action="{{url('chatstore/'.$id)}}" method="POST" class="hidden">
                <input type="text" data-conv-question="{{$result[0]->message}}" data-no-answer="true">
                <input type="text" name="name" data-conv-question="Please Tell me your name">
            </form>
        </div>
    </div>
    @else
        <h2>{{$message}}</h2>
    @endif
</div>

@endsection
@push('script')
{{-- <script type="text/javascript" src="{{ asset('jquery-1.12.3.min.js')}}"></script> --}}
<script type="text/javascript" src="{{ asset('dist/autosize.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('dist/jquery.convform.js')}}"></script>
@endpush


@push('script')
<script>
    var count = 0;
    var status = 'in_progress';
    var url = "{{route('chatbotanswer')}}"
    $(function(){
        // $("#login").click(function(){
        //     $.ajax({
        //             type: 'POST',
        //             url: ,
        //             data: data,
        //             success: function(response){

        //             }
        // });
        var loginStatus = false;
        var registrationStatus = false;
        var loginCount = 0;
        var registrationCount = 0;
        var formdata = {};
        var convForm = $('#chat').convform(
            {eventList:{onInputSubmit: function(convState, ready) {

            console.log('input is being submitted...');
            console.log(convState);
            var name = convState.current.input.name;
            if(name === 'name')
                formdata.name = convState.current.answer.value;

            if(loginStatus || registrationStatus){
                if(name == 'email')
                    formdata.email = convState.current.answer.value;
                if(name == 'password')
                    formdata.password = convState.current.answer.value;
            }
            console.log(!loginStatus && !registrationStatus);
            if(!loginStatus && !registrationStatus){
                if(status == 'in_progress'){
                    convState.current.next  = true;

                    var data = {
                        sessionId : '{{$sessionId}}',
                        _token : '<?php echo csrf_token() ?>',
                        answer : convState.current.answer.text
                    }
                    setTimeout(ready, Math.random()*500+100);
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: data,
                        success: function(response){
                            if(response.message == ''){
                                var result = JSON.parse(response.result)
                                result = result[0];
                                console.log(result.type,result.message,result.options);
                                if(result.message != 'login' && result.message != 'register'){
                                    if(result.type == 'button')
                                    {
                                        var message = result.message
                                        let newArr = result.options.map((val, index, arr) => {
                                            return {value:val,text:val}
                                        });
                                        var jsonFornewState = {
                                            type: 'select',
                                            name: message,
                                            questions: [message],
                                            answers:newArr,

                                        }
                                    }
                                    if(result.type == 'text'){

                                        var message = result.message
                                        var jsonFornewState = {
                                            type: 'text',
                                            questions: [message],
                                        }
                                    }


                                    if(response.status == 'completed'){
                                        jsonFornewState.noanswer = "true";
                                        status  = 'completed';
                                    }
                                    convState.current.next = convState.newState(jsonFornewState);
                                }else{
                                    if(result.message == 'login'){
                                        // $("#loginModelButton")
                                        // $('#loginModel').modal('show');
                                        // $( "#loginModelButton" ).trigger( "click" );
                                        var jsonFornewState = {
                                            type: 'email',
                                            questions: ["Type in your e-mail"],
                                            name:'email',
                                            pattern:["^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$"]
                                        }
                                        loginCount++;
                                        loginStatus = true;
                                    }
                                    if(result.message == 'register'){
                                        // $( "#registrationModelButton" ).trigger( "click" );
                                        var jsonFornewState = {
                                            type: 'email',
                                            questions: ["Type in your e-mail"],
                                            name:'email',
                                            pattern:["^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$"]
                                        }
                                        registrationCount++;


                                        registrationStatus = true;
                                    }
                                    convState.current.next = convState.newState(jsonFornewState);
                                }
                            }else{
                                alert(response.message)
                            }
                        },

                        async:false
                    });

                    }else{

                        convState.current.next = false;
                        console.log('dfd');
        // 	//emulating random response time (100-600ms)
                        setTimeout(ready, Math.random()*500+100);
                    }
                }else{
                    if(loginStatus){
                        convState.current.next  = true;
                        // <input data-conv-question="Type in your e-mail" data-pattern="^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$" id="email" type="email" name="email" required placeholder="What's your e-mail?">

                        if(loginCount == 1){
                            var jsonFornewState = {
                                type: 'password',
                                questions: ["Please Enter your Password"],
                                name:'password'
                            }
                            convState.current.next = convState.newState(jsonFornewState);
                            setTimeout(ready, Math.random()*500+100);
                        }
                            console.log('dfjdsddd');
                        if(loginCount == 2){
                            formdata. _token = '<?php echo csrf_token() ?>';
                            formdata.mode = 'login';
                            $.ajax({
                                type: 'POST',
                                url: "{{url('checklogin')}}",
                                data: formdata,
                                success: function(response){
                                    if(response.status){
                                        var data = {
                                            sessionId : '{{$sessionId}}',
                                            _token : '<?php echo csrf_token() ?>',
                                            answer : response.message
                                        }
                                        // convState.current.next = convState.newState({
                                        //     type: 'select',
                                        //     name: 'dynamic-question-'+count,
                                        //     questions: ['This question state was built on your previous answer (you answered: )'],
                                        //     answers: [
                                        //         {text: 'Answer 1', value: '1'},
                                        //         {text: 'Answer 2', value: '2'},
                                        //         {text: 'END', value: 'end'}
                                        //     ]
                                        // });
                                    }else{
                                        alert(response.result)
                                        location.reload();
                                        var data = {
                                            sessionId : '{{$sessionId}}',
                                            _token : '<?php echo csrf_token() ?>',
                                            answer : response.message
                                        }
                                    }
                                    $.ajax({
                                            type: 'POST',
                                            url: url,
                                            data: data,
                                            success: function(response){
                                                var result = JSON.parse(response.result)
                                                result = result[0];
                                                if(result.type == 'button'){
                                                    var message = result.message
                                                    let newArr = result.options.map((val, index, arr) => {
                                                        return {value:val,text:val}
                                                    });
                                                    var jsonFornewState = {
                                                        type: 'select',
                                                        name: message,
                                                        questions: [message],
                                                        answers:newArr,

                                                    }
                                                }
                                                loginStatus = false;
                                                if(result.type == 'text'){

                                                    var message = result.message
                                                    var jsonFornewState = {
                                                        type: 'text',
                                                        questions: [message],
                                                    }
                                                }
                                                convState.current.next = convState.newState(jsonFornewState);
                                                setTimeout(ready, Math.random()*500+100);
                                            }
                                        });

                                }


                            });

                        }
                        loginCount++;


                    }else{
                        convState.current.next  = true;
                        if(registrationCount == 1){
                            var jsonFornewState = {
                                type: 'password',
                                questions: ["Please Enter your Password"],
                                name:'password'
                            }
                            convState.current.next = convState.newState(jsonFornewState);
                            setTimeout(ready, Math.random()*500+100);
                        }

                        if(registrationCount == 2){
                               formdata. _token = '<?php echo csrf_token() ?>';
                               formdata.mode = 'register';
                            $.ajax({
                                type: 'POST',
                                url: "{{url('checklogin')}}",
                                data: formdata,
                                success: function(response){
                                    if(response.status){
                                        var data = {
                                            sessionId : '{{$sessionId}}',
                                            _token : '<?php echo csrf_token() ?>',
                                            answer : response.message
                                        }
                                        // convState.current.next = convState.newState({
                                        //     type: 'select',
                                        //     name: 'dynamic-question-'+count,
                                        //     questions: ['This question state was built on your previous answer (you answered: )'],
                                        //     answers: [
                                        //         {text: 'Answer 1', value: '1'},
                                        //         {text: 'Answer 2', value: '2'},
                                        //         {text: 'END', value: 'end'}
                                        //     ]
                                        // });
                                    }else{
                                        alert(response.result)
                                        location.reload();
                                        var data = {
                                            sessionId : '{{$sessionId}}',
                                            _token : '<?php echo csrf_token() ?>',
                                            answer : response.message
                                        }
                                    }
                                    $.ajax({
                                            type: 'POST',
                                            url: url,
                                            data: data,
                                            success: function(response){
                                                var result = JSON.parse(response.result)
                                                result = result[0];
                                                if(result.type == 'button'){
                                                    var message = result.message
                                                    let newArr = result.options.map((val, index, arr) => {
                                                        return {value:val,text:val}
                                                    });
                                                    var jsonFornewState = {
                                                        type: 'select',
                                                        name: message,
                                                        questions: [message],
                                                        answers:newArr,

                                                    }
                                                    console.log(jsonFornewState);
                                                }
                                                registrationStatus = false;
                                                if(result.type == 'text'){

                                                    var message = result.message
                                                    var jsonFornewState = {
                                                        type: 'text',
                                                        questions: [message],
                                                    }
                                                }
                                                setTimeout(ready, Math.random()*500+100);
                                                convState.current.next = convState.newState(jsonFornewState);
                                                setTimeout(ready, Math.random()*500+100);
                                            }
                                        });

                                },

                        async:false


                            });

                        }

                        registrationCount++;
                        convState.current.next = convState.newState(jsonFornewState);
                        setTimeout(ready, Math.random()*500+100);
                    }
                }
        },
         onSubmitForm: function(convState) {
             //alert('hrere')
         }
        }});
    });
</script>
@endpush
