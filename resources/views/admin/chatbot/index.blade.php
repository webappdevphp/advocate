@extends('layouts.app')
@section('page-heading','Chatbot')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Chatbot Response
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="row" >
                    <div class="col-lg-9">

                    </div>
                    <div class="col-lg-3">
                        {!! Form::open(['method' => 'GET', 'url' => '/admin/chatbot-response', 'class' => 'float-left', 'role' => 'search'])  !!}
                            <div class="form-group col-lg-9">
                                <input type="text"  class="form-control input-sm" name="search"  style="height:auto"placeholder="Search...">

                            </div>
                            <span class="input-group-append col-lg-3">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>ID</th><th>Name</th><th>Topic</th><th>Email</th><th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($chatbots as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>
                                        <a href="{{ url('/admin/chatbot-response', $item->id) }}">{{ $item->name }}</a>
                                    </td>
                                    <td>{{ $item->topic }}</td>
                                    <td>{{ $item->email }}</td>

                                    <td>
                                        <a href="{{ url('/admin/chatbot-response/' . $item->id) }}" title="View Advertisement"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                        {{-- <a href="{{ url('/admin/advertisement/' . $item->id . '/edit') }}" title="Edit Advertisement"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                        {!! Form::open([
                                            'method' => 'DELETE',
                                            'url' => ['/admin/advertisement', $item->id],
                                            'style' => 'display:inline'
                                        ]) !!}
                                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                    'type' => 'submit',
                                                    'class' => 'btn btn-danger btn-sm',
                                                    'title' => 'Delete User',
                                                    'onclick'=>'return confirm("Confirm delete?")'
                                            )) !!}
                                        {!! Form::close() !!} --}}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="pagination"> {!! $chatbots->appends(['search' => Request::get('search')])->render() !!} </div>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
</div>


@endsection

{{--

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="span9">
                @include('navigation.alert')
                <div class="module">
                    <div class="module-head">Chatbot Response</div>
                    <div class="module-body table">

                        {!! Form::open(['method' => 'GET', 'url' => '/admin/chatbot-response', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search"  style="height:auto"placeholder="Search...">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>ID</th><th>Name</th><th>Topic</th><th>Email</th><th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($chatbots as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>
                                            <a href="{{ url('/admin/chatbot-response', $item->id) }}">{{ $item->name }}</a>
                                        </td>
                                        <td>{{ $item->topic }}</td>
                                        <td>{{ $item->email }}</td>

                                        <td>
                                            <a href="{{ url('/admin/chatbot-response/' . $item->id) }}" title="View Advertisement"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            {{-- <a href="{{ url('/admin/advertisement/' . $item->id . '/edit') }}" title="Edit Advertisement"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/advertisement', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => 'Delete User',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!} -}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination"> {!! $chatbots->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection --}}
