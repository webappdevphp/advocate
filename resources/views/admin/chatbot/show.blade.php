@extends('layouts.app')
@section('page-heading','Chatbot')
@push('css')
    <style type="text/css">
        .bold{
            font-weight: bold;
        }
    </style>
@endpush
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                View Chatbot Response
            </div>
        </div>

        <div class="panel-body">
                        <a href="{{ url('/admin/chatbot-response') }}" class="btn btn-success btn-sm" title="Chatbot List">
                            Back
                        </a>
                        @php
                        $responses  = json_decode($chatbot->response);
                        @endphp
                        @foreach($responses as $key => $response)
                            <h2><?php echo wordwrap(ucfirst(str_replace('_',' ',$key))) ?> : <small>{{$response}}</small></h2>
                            <hr>
                        @endforeach


                    </div>
                </div>
            </div>
            <!--/.content-->
        </div>
    </div>
</div>
@endsection
