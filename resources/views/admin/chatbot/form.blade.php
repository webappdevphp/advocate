<div class="control-group{{ $errors->has('name') ? ' has-error' : ''}}">
    {!! Form::label('name', 'Name ', ['class' => 'control-label']) !!}
    <div class="controls">
        {!! Form::text('name', null, ['class' => 'span8','placeholder'=>'Type something here...','required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="control-group{{ $errors->has('jobtitle') ? ' has-error' : ''}}">
    {!! Form::label('jobtitle', 'Job Title ', ['class' => 'control-label']) !!}
    <div class="controls">
        {!! Form::text('jobtitle', null, ['class' => 'span8','placeholder'=>'Type something here...','required']) !!}
        {!! $errors->first('jobtitle', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="control-group{{ $errors->has('image') ? ' has-error' : ''}}">
    {!! Form::label('image', 'Image ', ['class' => 'control-label']) !!}
    <div class="controls">
		{!! Form::hidden('old_image', isset($advertise->image)?$advertise->image:null, []) !!}
        {!! Form::file('image', null, ['class' => 'span8']) !!}
        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="control-group{{ $errors->has('description') ? ' has-error' : ''}}">
    {!! Form::label('description', 'Description ', ['class' => 'control-label']) !!}
    <div class="controls">
        {!! Form::textarea('description', null, ['class' => 'span8','placehoder'=>'Type something here...','rows'=>'5']) !!}
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div>