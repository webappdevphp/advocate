{{-- <div class="span3">
    <div class="sidebar">
        <ul class="widget widget-menu unstyled">
            @foreach($laravelAdminMenus->menus as $section)
                @if($section->items)
                    <li>
                        <a class="collapsed" data-toggle="collapse" href="#{{ $section->section }}">
                            <i class="menu-icon icon-cog"></i>
                            <i class="icon-chevron-down pull-right"></i>
                            <i class="icon-chevron-up pull-right"></i>
                            {{ $section->section }}
                        </a>
                        <ul id="{{ $section->section }}" class="collapse unstyled">
                            @foreach($section->items as $menu)
                                <li><a href="{{ url($menu->url) }}"><i class="icon-inbox"></i>{{ $menu->title }} </a></li>
                            @endforeach
                        </ul>
                    </li>
            {{-- <div class="card">
                <div class="card-header">

                </div>

                <div class="card-body">
                    <ul class="nav flex-column" role="tablist">
                        @foreach($section->items as $menu)
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" href="{{ url($menu->url) }}">
                                    {{ $menu->title }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div> -}}

                @endif
            @endforeach
        </ul>
    </div>
</div> --}}
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            @foreach($laravelAdminMenus->menus as $section)
                @if($section->items)

                    <li>
                        <a href="#"><i class="fa fa-tasks"></i> {{ $section->section }}<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            @foreach($section->items as $menu)
                                <li><a href="{{ url($menu->url) }}">{{ $menu->title }} </a></li>
                            @endforeach
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>

                @endif
            @endforeach
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
