@extends('layouts.app')
@section('page-heading','Chatbot')
@push('css')
    <style type="text/css">
        .bold{
            font-weight: bold;
        }
    </style>
@endpush
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                View Chatbot Response
            </div>
        </div>
        <div class="panel-body">
            <a href="{{ url('/admin/attorny-request-list') }}" class="btn btn-success btn-sm" title="Advertise List">
                Back
            </a>
            <h2>User Name : <small>{{$requesAttorney->name}}</small></h2>
            <hr>
            <h2>Allow Share Detail : <small>{{($requesAttorney->allowtosharedetail)? 'Yes' : 'No'}}</small></h2>
            <hr>

            {{-- str_replace(' ', '', $sentence); --}}
            @php
            $responses  = json_decode($requesAttorney->response);
            @endphp
            @foreach($responses as $key => $response)
                <h2><?php echo wordwrap(ucfirst(str_replace('_',' ',$key))) ?> : <small>{{$response}}</small></h2>
                <hr>
            @endforeach
{{--
                        <div class="control-group{{ $errors->has('image') ? ' has-error' : ''}}">
                            {!! Form::label('image', 'Image :', ['class' => 'control-label bold']) !!}
                            <img src="{{ asset('uploads/'.$advertise->image) }}" height="50px" width="50px">
                        </div> --}}
{{--
                        <div class="control-group{{ $errors->has('description') ? ' has-error' : ''}}">
                            {!! Form::label('description', 'Description :', ['class' => 'control-label bold']) !!}
                            {!! Form::label($advertise->description, $advertise->description , ['class' => 'control-label']) !!}
                        </div> --}}
                    </div>
                </div>
            </div>
            <!--/.content-->
        </div>
    </div>
</div>
@endsection
