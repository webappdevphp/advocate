@extends('layouts.app')
@section('page-heading','Reqeust Attorney')
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('navigation.alert')
        <div class="panel panel-default">
            <div class="panel-heading">
                Request For Attorney
            </div>
            <div class="panel">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>User Name</th>
                                <th>Category</th>
                                <th>Share Detail</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($requesAttorney as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>
                                    {{ $item->name }}
                                </td>
                                <td>{{ $item->category_id }}</td>
                                <td>{{ ($item->allowtosharedetail) ? 'Yes' : 'No' }}</td>

                                <td>
                                    <a href="{{ url('/admin/attorny-request-detail/' . $item->id) }}" title="View Request"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                    {{-- <a href="{{ url('/admin/advertisement/' . $item->id . '/edit') }}" title="Edit Advertisement"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                    {!! Form::open([
                                        'method' => 'DELETE',
                                        'url' => ['/admin/advertisement', $item->id],
                                        'style' => 'display:inline'
                                    ]) !!}
                                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                'type' => 'submit',
                                                'class' => 'btn btn-danger btn-sm',
                                                'title' => 'Delete User',
                                                'onclick'=>'return confirm("Confirm delete?")'
                                        )) !!}
                                    {!! Form::close() !!} --}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="pagination"> {!! $requesAttorney->appends(['search' => Request::get('search')])->render() !!} </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection
