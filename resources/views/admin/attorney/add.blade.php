@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="span9">
        	@include('navigation.alert')
            <div class="content">
                <div class="module">
                    <div class="module-head">
                        <h3>
                            Add Advertisement
                        </h3>

                    </div>
                    <div class="module-body">
                        <a href="{{ url('/admin/advertisement') }}" class="btn btn-success btn-sm" title="Advertise List">
                            Back
                        </a>
							{!! Form::open(['url' => '/admin/advertisement', 'class' => 'form-horizontal row-fluid','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}

                                @include('admin.advertisement.form')


		                        <div class="control-group">
		                            <div class="controls">
		                                <button class="btn btn-primary" type="submit">
		                                    Add
		                                </button>
		                            </div>
		                        </div>
		                    {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <!--/.content-->
        </div>
    </div>
</div>
@endsection
