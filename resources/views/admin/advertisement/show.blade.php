@extends('layouts.app')
@section('page-heading','Advertisment')
@push('css')
    <style type="text/css">
        .bold{
            font-weight: bold;
        }
    </style>
@endpush
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                View Advertisement
            </div>
        </div>

        <div class="panel-body">
                        <a href="{{ url('/admin/advertisement') }}" class="btn btn-success btn-sm" title="Advertise List">
                            Back
                        </a>
                        <h2>Name : <small>{{$advertise->name}}</small></h2>
                        <h2>Job Title : <small>{{$advertise->jobtitle}}</small></h2>
                        <h2>Image : <small><img src="{{ asset('uploads/'.$advertise->image) }}" height="50px" width="50px"></small></h2>
                        <h2>Description : <small>{{$advertise->description}}</small></h2>

                    </div>
                </div>
            </div>
            <!--/.content-->
        </div>
    </div>
</div>
@endsection
