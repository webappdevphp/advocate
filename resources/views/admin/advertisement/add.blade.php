@extends('layouts.app')
@section('page-heading','Advertisment')
@section('content')
<div class="row">
    @include('navigation.alert')
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Add Advertisement
                <a href="{{ url('/admin/advertisement') }}" class="btn btn-success btn-sm" title="Advertise List">
                    Back
                </a>
            </div>
            <div class="panel-body">


                <div class="row" />
                    <div class="col-lg-12">
                        {!! Form::open(['url' => '/admin/advertisement', 'class' => '','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}

                            @include('admin.advertisement.form')


                            <div class="control-group">
                                <div class="controls">
                                    <button class="btn btn-primary" type="submit">
                                        Add
                                    </button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
            <!--/.content-->
</div>

@endsection
