@extends('layouts.app')
@section('page-heading','Advertisment')
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('navigation.alert')
        <div class="panel panel-default">
            <div class="panel-heading">
                Advertisement List
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="row" >
                    <div class="col-lg-9">
                        <a href="{{ url('/admin/advertisement/create') }}" class="btn btn-success btn-sm" title="Add New Advertise">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                    </div>
                    <div class="col-lg-3">
                        {!! Form::open(['method' => 'GET', 'url' => '/admin/advertisement', 'class' => 'float-left', 'role' => 'search'])  !!}
                            <div class="form-group col-lg-9">
                                <input type="text"  class="form-control input-sm" name="search"  style="height:auto"placeholder="Search...">

                            </div>
                            <span class="input-group-append col-lg-3">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>ID</th><th>Name</th><th>Job Title</th><th>Image</th><th>Action</th>
                            </tr>
                        </thead>
                            <tbody>
                            @foreach($advertise as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td><a href="{{ url('/admin/advertisement', $item->id) }}">{{ $item->name }}</a></td><td>{{ $item->jobtitle }}</td>
                                <td><img src="{{ asset('uploads').'/'.$item->image}}" width="100px" height="100px" /></td>
                                    <td>
                                        <a href="{{ url('/admin/advertisement/' . $item->id) }}" title="View Advertisement"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                        <a href="{{ url('/admin/advertisement/' . $item->id . '/edit') }}" title="Edit Advertisement"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                        {!! Form::open([
                                            'method' => 'DELETE',
                                            'url' => ['/admin/advertisement', $item->id],
                                            'style' => 'display:inline'
                                        ]) !!}
                                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                    'type' => 'submit',
                                                    'class' => 'btn btn-danger btn-sm',
                                                    'title' => 'Delete User',
                                                    'onclick'=>'return confirm("Confirm delete?")'
                                            )) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
</div>


@endsection
