@extends('layouts.front')

@section('title','Home')

@section('content')
<div class="fw-auth">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<a href="{{url('/')}}">
					<i class="fa fa-angle-left"></i> Back to Home
				</a>
				<div class="fw-auth-section whiteback" style="min-height: 500px;">
					<h1>Attorney Detail </h1>
					<ul id="topicList">
						@foreach($attorneyList as $key => $attorney)
							<li id="{{$key}}">
                                {{-- <a href="{{url('topic/'.$topic->id)}}" tittle={{$topic->name}}>{{$topic->name}}</a> --}}
                            <span>{{$attorney->fname}} {{$attorney->lname}}</span>
                            <br />
                            <span>{{$attorney->email}}</span>
							</li>
						@endforeach
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
