@extends('layouts.front')

@section('title','Request Attorny')
@push('css')
<link type="text/css" href="{{ asset('dist/jquery.convform.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('dist/chatbot.custom.css')}}">
@endpush
@section('content')
<div class="fw-auth">
    <div class="container">
        <div class="row">
			<div class="module col-md-12" style="min-height: 500px">
				<div class="subtitle">
					{{-- <h3>Topic: {{$detail->name}}</h3> --}}
				</div>
				<div>
					<div class="card no-border chatapp">
						<div id="chat" class="conv-form-wrapper">
                            <form action="{{route('attorny-request')}}" method="POST" class="hidden">
                                <?php
                                foreach($questions as $key=>$value){
                                    if($value['type'] == 'select'){
                                        $option = '';
                                ?>
									<select data-conv-question="{{$value['question']}}" name="{{$value['question']}}">
										<?php
										foreach($value['options'] as $keyQuestion => $valueQuestion){
											$option .= '<option value="'.$keyQuestion.'">'.$valueQuestion.'</option>';
										}
										echo $option;
										?>
									</select>
                                <?php
                                    }
                                    if($value['type'] == 'text'){
                                        echo '<input type="text" name="'.$value['question'].'" data-conv-question="'.$value['question'].'">';
                                    }
                                    if($value['type'] == 'noresponse'){
                                        echo '<input type="text" data-conv-question="'.$value['question'].'" data-no-answer="true">';
                                    }
                                    if($value['type'] == 'email'){
                                        echo '<input data-conv-question="'.$value['question'].'" data-pattern="^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$" id="email" type="email" name="email" required placeholder="'.$value['question'].'">';
                                    }
                                }
                                ?>
							</form>
						</div>
					</div>
				</div>
			</div>
            <div id="acceptAttorney" class="module col-md-12" style="display:none">
                <div class="fw-auth-section whiteback" style="min-height: 200px;">
                    <h1>Accept Term For Requesting Attorney</h1>

                    <form id="acceptTerms" action="{{ route('accept-term') }}"  accept-charset="UTF-8" method="post">
                        @csrf
                        <input type="hidden" name="attorneyRequestId" id="attorneyRequestId" value='<?php echo (isset($id) && $id != null) ? $id : ""; ?>' />

                        <label class="form-group fw-remember-me-label custom-control custom-checkbox">
                            {{-- <input name="user[remember_me]" type="hidden" value="0"><input class="custom-control-input" type="checkbox" value="1" name="user[remember_me]" id="user_remember_me"> --}}
                            <input class="custom-control-input" required  type="checkbox" name="terms" id="terms"
                            >
                            <span class="custom-control-indicator"></span>
                            <span for="terms" class="custom-control-description">Accept Terms and Condition</span>
                            <!--<label for="user_remember_me">Remember me</label>-->
                        </label>

                        <label class="form-group fw-remember-me-label custom-control custom-checkbox">
                            {{-- <input name="user[remember_me]" type="hidden" value="0"><input class="custom-control-input" type="checkbox" value="1" name="user[remember_me]" id="user_remember_me"> --}}
                            <input class="custom-control-input" type="checkbox" name="allowtosharedetail" id="allow"

                            >
                            <span class="custom-control-indicator"></span>
                            <span for="allow" class="custom-control-description">Allow to send Your Contact Information To Respecitve Attorney</span>
                            <!--<label for="user_remember_me">Remember me</label>-->
                        </label>

                        <div class="text-right">
                            <input type="submit" name="submit" id="submitTerms" value="Submit" class="btn btn-primary fw-btn-auth" >
                        </div>
                    </form>
                </div>
            </div>
            <div id="attorneyListDiv" class="module col-md-12" style="display:none">
                <div class="fw-auth-section whiteback" style="min-height: 250px;">
                    <h1>Attorney Detail </h1>
                    <ul id="attorneyList">
                    </ul>
                </div>
            </div>
	    </div>
	</div>
	</div>

@endsection
@push('script')
{{-- <script type="text/javascript" src="{{ asset('jquery-1.12.3.min.js')}}"></script> --}}
<script type="text/javascript" src="{{ asset('dist/autosize.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('dist/jquery.convform.js')}}"></script>
@endpush


@push('script')
<script>

    $(function(){
        function SubmitForm(){
            var inputElem = document.createElement('input');
            inputElem.type = 'hidden';
            inputElem.name = '_token';
            inputElem.value = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
            $('.hidden').append(inputElem);
            var formData = $('.hidden').serialize();
            var url = $('.hidden').attr("action")
            console.log(formData,url);
            $.ajax({
                type:"POST",
                url:url,
                data:formData,
                dataType: 'json',
                success: function(data){
                    if(data.status){
                        $("#attorneyRequestId").val(data.requestAttorneyId);
                        $("#acceptAttorney").show();
                    }else{
                        alert('Please Try again');
                    }
                },
                error: function(data){

                }
            })
        }
        $("#acceptTerms").on( 'submit', function(e) {
            e.preventDefault();
			$("#submitTerms").attr("disabled", true);
            var formData = $(this).serialize();
            var url = $(this).attr("action")
            console.log(formData,url);
            $.ajax({
                type:"POST",
                url:url,
                data:formData,
                dataType: 'json',
                success: function(data){
                    if(data.status){
                        var attorneyList = data.attorneyList;
                        console.log(attorneyList.length);
                        var attorneyListLi = '';
                        if(attorneyList.length > 0){
                            for(i=0;i < attorneyList.length;i++){
                                 attorneyListLi = attorneyListLi + '<li><i class="fa fa-user"></i><span style="padding-left: 5px;">'+attorneyList[i].fname+' '+attorneyList[i].lname+'</span><br /><i class="fa fa-envelope"></i><span style="padding-left: 5px;">'+attorneyList[i].email+'</span><br /><i class="fa fa-phone"></i><span style="padding-left: 5px;">'+attorneyList[i].phone+'</span><br /><i class="fa fa-user"></i><span style="padding-left: 5px;">'+attorneyList[i].position+'</span></li>'
                                //console.log(attorneyList.length)
                            }
                            $("#attorneyList").html(attorneyListLi);
                            $("#attorneyListDiv").show();
                        }else{
                            alert('Please Try Again');
                        }
                        // attorneyList.map(attorney =>{

                        //     return '<li><span>'+attorney.fname+' '+attorney.lname+'</span><br /><span>'+attorney.email+'</span><br /><span>'+attorney.phone+'</span><br /><span>'+attorney.position+'</span></li>'
                        // })

                    }else{
                        alert('Please Try again');
                    }
                },
                error: function(data){

                }
            })

        });
        var convForm = $('#chat').convform(
            {
                eventList:{
                    onSubmitForm: function(convState,ready) {
                        setTimeout(ready, Math.random()*500+100);
                        console.log('The form is being submitted!');
                        var placeHolder = setInterval(function() {
                            // var inputElem = document.createElement('input');
                            // inputElem.type = 'hidden';
                            // inputElem.name = '_token';
                            // inputElem.value = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
                            // convState.form.append(inputElem)
                            //convState.form.submit();
                            SubmitForm();
                            $('#userInput').prop("disabled", true);
                            clearInterval(placeHolder);
                        }, 1000);

                    }
                }
            }
        );
    });
</script>
@endpush
