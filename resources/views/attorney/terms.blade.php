@extends('layouts.front')

@section('title','Request Attorny Terms')
@push('css')
<link type="text/css" href="{{ asset('dist/jquery.convform.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('dist/chatbot.custom.css')}}">
@endpush
@section('content')
<div class="fw-auth">
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-md-3">
              {{-- <a href="https://www.freewill.com/"> --}}
                <a href="{{url ('/')}}">
                    <i class="fa fa-angle-left"></i> Back to Home
                </a>
                <div class="fw-auth-section">
                    <h1>Accept Term For Requesting Attorney</h1>

                    <form action="{{ route('accept-term') }}"  accept-charset="UTF-8" method="post">
                        @csrf
                        <input type="hidden" name="attorneyRequestId" value={{$id}} />

                        <label class="form-group fw-remember-me-label custom-control custom-checkbox">
                            {{-- <input name="user[remember_me]" type="hidden" value="0"><input class="custom-control-input" type="checkbox" value="1" name="user[remember_me]" id="user_remember_me"> --}}
                            <input class="custom-control-input" required  type="checkbox" name="terms" id="terms"
                                <?php
                                    if($requestAttorney->terms)
                                        echo 'checked';
                                ?>
                            >
                            <span class="custom-control-indicator"></span>
                            <span for="terms" class="custom-control-description">Accept Terms and Condition</span>
                            <!--<label for="user_remember_me">Remember me</label>-->
                        </label>

                        <label class="form-group fw-remember-me-label custom-control custom-checkbox">
                            {{-- <input name="user[remember_me]" type="hidden" value="0"><input class="custom-control-input" type="checkbox" value="1" name="user[remember_me]" id="user_remember_me"> --}}
                            <input class="custom-control-input" type="checkbox" name="allowtosharedetail" id="allow"
                            <?php
                                    if($requestAttorney->allowtosharedetail)
                                        echo 'checked';
                                ?>
                            >
                            <span class="custom-control-indicator"></span>
                            <span for="allow" class="custom-control-description">Allow to send Your Contact Information To Respecitve Attorney</span>
                            <!--<label for="user_remember_me">Remember me</label>-->
                        </label>

                        <div class="text-right">
                            <input type="submit" name="submit" value="Submit" class="btn btn-primary fw-btn-auth" >
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
