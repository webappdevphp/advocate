@extends('layouts.front')

@section('title','Home')
@section('content')
<div class="fw-auth">
    <div class="container">
        <div class="jumbotron text-xs-center">
        <h1 class="display-3">Thank You!</h1>
        <p class="lead">
        <hr>
        <p class="lead">
            <a class="btn btn-primary btn-sm" href="{{url('/')}}" role="button">Continue to homepage</a>
        </p>
        </div>
    </div>
</div>
@endsection
