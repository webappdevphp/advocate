@extends('layouts.front')
@section('title','Login')

@section('body_class','fw-auth-container')
@section('content')
<div class="fw-auth">
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-md-1">
              {{-- <a href="https://www.freewill.com/"> --}}
                <a href="{{url ('/')}}">
					<i class="fa fa-angle-left"></i> Back to Home
				</a>
				<a style="color: #FFF;" class="pull-right" href="{{url('/login')}}">Login</a>
      <div class="fw-auth-section">
      <h1>Register Here</h1>
		<?php
		$nameGet = '';
		if(isset($_GET['name']) && $_GET['name'] != ''){
			$nameGet = $_GET['name'];
		}
		
		?>
      <form class="new_user" id="new_user" action="{{ route('register') }}"  accept-charset="UTF-8" method="post">
        @csrf
		
		<div class="form-group">
			<label for="name">{{ __('Name') }}</label><br>
			<?php if($nameGet != ''){ ?>
			<input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="<?php echo $nameGet ?>" required autofocus>
			<?php }else {?>
			<input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{old('name')}}" required autofocus>
			<?php } ?>
			@if ($errors->has('name'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
			@endif
		</div>
		
		<div class="form-group">
			<label for="email">{{ __('E-Mail Address') }}</label><br>
			<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
			@if ($errors->has('email'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('email') }}</strong>
				</span>
			@endif
		</div>
		
		<div class="form-group">
			<label for="password">{{ __('Password') }}</label><br>
			<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

			@if ($errors->has('password'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('password') }}</strong>
				</span>
			@endif
		</div>
		
		<div class="form-group">
			<label for="password-confirm">{{ __('Confirm Password') }}</label><br>
			<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
		</div>

		<div class="text-right">
			<button type="submit" class="btn btn-primary">
				{{ __('SignIn') }}
			</button>
		</div>
      </form>
  </div>
            <div class="helpful-links">

                {{-- <a href="signup.html">Sign up</a> 
                <a href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                </a>--}}
                {{-- <a href="for_got_password.html">Forgot your password?</a> --}}
            </div>

            </div>
          </div>
        </div>
      </div>

@endsection
