@extends('layouts.front')

@section('title','Login')

@section('body_class','fw-auth-container')

@section('content')
<div class="fw-auth">
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-md-3">
              {{-- <a href="https://www.freewill.com/"> --}}
                 <a href="{{url ('/')}}">
                <i class="fa fa-angle-left"></i> Back to Home
              </a>
			  <a style="color: #FFF;" class="pull-right" href="{{url ('/register')}}">
                Register Now
              </a>
      <div class="fw-auth-section">
      <h1>Log in</h1>
      <form class="new_user" id="new_user" action="{{ route('login') }}"  accept-charset="UTF-8" method="post">
        @csrf
        @if(Session::has('flash_error'))
            <p class="alert alert-danger">{{ Session::get('flash_error') }}</p>
        @endif
        @if(Session::has('flash_success'))
            <p class="alert alert-success">{{ Session::get('flash_success') }}</p>
        @endif
      <input type='hidden' name="redirectback" value="{{str_replace(url('/'), '', url()->previous())}}" />
        <div class="form-group">
            <label for="email">Email</label><br>
            <input id="email" type="email" autofocus="autofocus" value="" placeholder="Enter Email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
            @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group">
          <label for="user_password">Password</label><br>

          <input id="password" autocomplete="off" type="password" placeholder="Password" value="" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
        </div>

          <label class="form-group fw-remember-me-label custom-control custom-checkbox">
            {{-- <input name="user[remember_me]" type="hidden" value="0"><input class="custom-control-input" type="checkbox" value="1" name="user[remember_me]" id="user_remember_me"> --}}
            <input class="custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
            <span class="custom-control-indicator"></span>
            <span class="custom-control-description">Remember me</span>
            <!--<label for="user_remember_me">Remember me</label>-->
          </label>

        <div class="text-right">
          <input type="submit" name="commit" value="Log in" class="btn btn-primary fw-btn-auth" data-disable-with="Log in">
        </div>
      </form>
  </div>

            <div class="helpful-links">

                {{-- <a href="signup.html">Sign up</a>
                <a href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                </a>--}}
                {{-- <a href="for_got_password.html">Forgot your password?</a> --}}
            </div>

            </div>
          </div>
        </div>
      </div>
{{-- <div class="module module-login span4 offset4">
    <form method="POST" action="{{ route('login') }}">
        @csrf
        <div class="module-head">
            <h3>Sign In</h3>
        </div>
        <div class="module-body">
            <div class="control-group">
                <div class="controls row-fluid">
                    <input id="email" type="email" placeholder="Enter Email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                </div>
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="control-group">
                <div class="controls row-fluid">
                    <input id="password" type="password" placeholder="Password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                </div>

                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="module-foot">
            <div class="control-group">
                <div class="controls clearfix">
                    <button type="submit" class="btn btn-primary pull-right">Login</button>
                    <label class="checkbox">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label class="form-check-label" for="remember">
                            {{ __('Remember Me') }}
                        </label>
                    </label>
                </div>
            </div>
        </div>
    </form>
</div> --}}
{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}
@endsection
