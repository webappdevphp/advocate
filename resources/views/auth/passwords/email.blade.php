@extends('layouts.app')

@section('content')
<div class="module module-login span4 offset4">
    <div class="module-head">
        <h3>{{ __('Reset Password') }}</h3>
    </div>
    <form method="POST" action="{{ route('password.email') }}">
        <div class="module-body">
        @if (session('status'))
            <p class="text-success">{{ session('status') }}</p>
        @endif
        @csrf

            <div class="control-group">
                <div class="controls row-fluid">
                    <input id="email" type="email" placeholder="Enter Email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                </div>
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="module-foot">
            <div class="control-group">
                <div class="controls clearfix">
                    <button type="submit" class="btn btn-primary pull-right">{{ __('Send Password Reset Link') }}</button>
                    
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
