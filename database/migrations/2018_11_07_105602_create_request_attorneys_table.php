<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestAttorneysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_attorneys', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('category_id');
            $table->longText('response');
            $table->integer('terms');
            $table->integer('allowtosharedetail');
            $table->string('attorney');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_attorneys');
    }
}
