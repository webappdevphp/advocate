<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::post('login', 'Auth\LoginController@authenticate')->name('login');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/thank-you', 'HomeController@thanks')->name('thank-you');

Route::get('/topic','TopicsController@index');
Route::post('/gettopic','TopicsController@index');
Route::get('/topic/{id}','TopicsController@chatbot');
Route::post('/chatbotanswer','TopicsController@chatbotanswer')->name('chatbotanswer');
Route::post('/chatstore/{id}','TopicsController@chatstore');
//Route::get('/attorny/{topic}/{id}','TopicsController@attorny');
Route::post('/checklogin','TopicsController@makeLogin');
Route::group(['middleware' => ['auth']],function(){
    Route::get('request-attorny','AttorneyController@index')->name('attorny-request');
    Route::post('request-attorny','AttorneyController@store')->name('attorny-request');
    Route::get('accept-term/{id}','AttorneyController@acceptTerm')->name('accept-term');
    Route::get('attorney-list/{id}','AttorneyController@attorneyList')->name('attorneyList');
    Route::post('accept-term','AttorneyController@acceptTermStore')->name('accept-term');

});
Route::group(['middleware' => ['auth', 'admin']], function () {
Route::get('admin', 'Admin\AdminController@index');
Route::get('admin/attorny-request-list', 'Admin\AdminController@listRequestAttorney');
Route::post('admin/attorny-request-list', 'Admin\AdminController@listRequestAttorney');
Route::get('admin/attorny-request-detail/{id}', 'Admin\AdminController@listRequestAttorneyDetail');
Route::resource('admin/roles', 'Admin\RolesController');
Route::resource('admin/permissions', 'Admin\PermissionsController');
Route::resource('admin/users', 'Admin\UsersController');
Route::resource('admin/advertisement', 'Admin\AdvertisementController');
Route::resource('admin/chatbot-response', 'Admin\ChatbotController');

Route::get('admin/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);
Route::post('admin/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);
});
